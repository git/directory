#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Free Software Foundation <info@fsf.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import sys

import export
import re

def filename(s, extension):
    s_ = re.sub('[^A-Za-z0-9_+.-]', '_', s)
    assert s_, s
    return s_ + '.' +extension

def output(path, xs):
    with open(path, 'w') as f:
            f.write(xs)

def main():
    data = export.PkgData()
    everything = {}

    for (name, templates) in export.export_all(data):
        page = []

        try:
            # Force errors.
            templates = list(templates)
        except export.ExportFailure, e:
            export.warn('export failed: %s: %s' % (name.encode('utf-8').strip(), e.message.encode('utf-8').strip()))

        for template in templates:
            tname = template.name
            values = dict(template.values)
            page.append((tname, values))

        if page != []:
            #name=page[0][1]['Name']
            print name
            fn=filename(name, 'json')
            data = json.dumps(page, sort_keys=True, indent=4, separators=(',', ': '))
            output('output/'+fn, data)

if __name__ == '__main__':
    main()


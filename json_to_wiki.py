#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Free Software Foundation <info@fsf.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import sys

import export

def main():
    everything = json.load(sys.stdin)

    def _export():
        for (name, valuess) in everything.iteritems():
            print name
            templates = []

            for (tname, values) in valuess:
                template = export.Template(
                    tname.decode('utf8'), values.items())
                templates.append(template)

            yield (name, templates)

    export.output_multi('converted', _export())

if __name__ == '__main__':
    main()


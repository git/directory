#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Free Software Foundation <info@fsf.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# summary of the License: fields found in Files: clauses

import pandas as pd

def histogram(values):
    hist = {}

    for v in values:
        v_ = v.lower()
        hist[v_] = hist.get(v_, 0) + 1

    return hist

if __name__ == '__main__':
    store = pd.HDFStore('cp.h5')
    cpf = store['cp_files']

    licenses = list(histogram(cpf['_license']))

    for (k, v) in sorted(licenses, key=lambda x: x[1], reverse=True):
        print '%-40s %6d' % (k.encode('utf8'), v)


I'm working as a volunteer for the Free Software Directory as an administrator. Your program
is free software so in principle it should be listed there, and I'd like to
add it.  But it has some problems in showing what its license is.
Would you please fix them, for the sake of users and other developers?

Once your version with fixed license issues is public available on  we will review it, and if it meet our requirement I will approve http://directory.fsf.org/wiki/Wayback_Machine. Once approved it will be listed on the official GNU IceCat add-on list at https://directory.fsf.org/wiki/IceCat. GNU IceCat is the GNU variant of Firefox.

See https://addons.mozilla.org/en-US/firefox/addon/librejs/ if you want to study a well licensed add-on.

# Issues


## No full copy of the license file included

There are no full copy of the license file

The root directory don't have a COPYING file with a copy of the software license. A plain text version of GPL 3.0 
can be found here: https://www.gnu.org/licenses/gpl-3.0.txt


# Release notes

Please mention in your release notes for the upcomming version that
your software now is properly licensed as free software.


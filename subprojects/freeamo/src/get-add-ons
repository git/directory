#!/usr/bin/env bash
#    This file is part of FreeAMO
#    Copyright (C) 2017 David Hedlund
#
#    FreeAMO is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    FreeAMO is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

cd "build/json/search-pages" || exit

function get-add-ons--download {


    output_file="$output_file_2";

    if [ "$debug" = true ]; then

        pv__size=$(cat "$output_file" | head -n 3 | wc -l)
        
    else
        
        pv__size="$(wc -l < "$output_file")"

    fi

    echo "Download $pv__size add-ons"
    
    while IFS= read -r table; do

        file=$(echo "$table" | awk '{print $1}');
        entry=$(echo "$table" | awk '{print $2}');

        source "$run_path/src/main_functions"
        cd "$run_path/build/json/search-pages" || exit
        amo_variables

        cd "$run_path/build/add-ons" || exit
        # Add-ons not avalible for GNU/Linux will be ignored (used to be very few though).
        wget -nc -q "https://addons.mozilla.org/firefox/downloads/latest/$slug/addon-$id-latest.xpi" || wget -nc -q "https://addons.mozilla.org/firefox/downloads/latest/$slug/platform:2/addon-$id-latest.xpi"

        if ! $debug; then echo "foo"; fi # for pv
        
    done < "$output_file" | pv__progress

}


function get-add-ons--filter {

    rm -f "$run_path/build/index-search-pages--free-licenses--license-file-status.txt"
    
    output_file="$output_file_2";

    if [ "$debug" = true ]; then

        pv__size=$(cat "$output_file" | head -n 3 | wc -l)
        
    else
        
        pv__size="$(wc -l < "$output_file")"

    fi

    echo "Filter $pv__size add-ons "
    
    while IFS= read -r table; do

        file=$(echo "$table" | awk '{print $1}');
        entry=$(echo "$table" | awk '{print $2}');

        source "$run_path/src/main_functions"
        cd "$run_path/build/json/search-pages" || exit
        amo_variables

        cd "$run_path/build/add-ons" || exit
        license_file_text=$(unzip -l "addon-$id-latest.xpi" | awk '{ print $4 }' | grep -E "^COPYING$|^COPYING.txt$|^COPYING.md$|^LICENSE$|^LICENSE.txt$|^LICENSE.md$" | sed ':a;N;$!ba;s/\n/\t/g' || echo "NOLICENSEFILEFOUND");
        echo -e "$file\\t$entry\\t$slug\\taddon-$id-latest.xpi\\t$license_file_text" >> "$run_path/build/index-search-pages--free-licenses--license-file-status.txt"
        
        if ! $debug; then echo "foo"; fi # for pv

    done < "$output_file" | pv__progress

}

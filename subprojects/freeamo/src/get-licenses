#!/usr/bin/env bash
#    This file is part of FreeAMO
#    Copyright (C) 2018 David Hedlund
#
#    FreeAMO is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    FreeAMO is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# API: Please adopt SPDX - https://github.com/mozilla/addons-server/issues/8706
# Allow GNU [L]GPL x.x or later - https://github.com/mozilla/addons-server/issues/8707

cd "build/json/search-pages" || exit

function get-licenses--download {

    # API: Add "last_license" to the search pages - https://github.com/mozilla/addons-server/issues/8945

    output_file="$output_file_1";
    
    if [ "$debug" = true ]; then

        pv__size=$(cat "$output_file" | head -n 3 | wc -l)
        
    else
        
        pv__size="$(wc -l < "$output_file")"

    fi

    source "$run_path/src/main_functions"

    echo "Download $pv__size current_versions files that contain license information."
            

    line=0
    while IFS= read -r table; do

        ((++line))
        
        if [ "$debug" = true ] && [ "$line" -le "3" ] || [ "$debug" = false ] ; then
            
            file=$(echo "$table" | awk '{print $1}');
            entry=$(echo "$table" | awk '{print $2}');

            source "$run_path/src/main_functions"
            amo_variables

            total=$(wc -l < "$output_file_1")
            #            echo -e "#==================
            #Entry $line (of $total): $name\n"

            # Do not download the same license files over and over
            wget -nc -q "https://addons.mozilla.org/api/v3/addons/addon/$slug/versions/$current_version__id/" -O "$run_path/build/json/current_versions/$current_version__id.json"

        fi

        if ! $debug; then echo "foo"; fi # for pv

    done < "$output_file_1" | pv__progress

}

function get-licenses--filter {

    output_file="$output_file_1";
    
    if [ "$debug" = true ]; then

        pv__size=$(cat "$output_file" | head -n 3 | wc -l)

    else

        pv__size="$(wc -l < "$output_file")"
        
    fi
    
    source "$run_path/src/main_functions"
    rm -f "$run_path/build/index-search-pages--free-licenses.txt"

    echo "Filter free licenses for $pv__size entries."

    line=0
    while IFS= read -r table; do

        ((++line))
        
        if [ "$debug" = true ] && [ "$line" -le "3" ] || [ "$debug" = false ] ; then
            
            file=$(echo "$table" | awk '{print $1}');
            entry=$(echo "$table" | awk '{print $2}');

            source "$run_path/src/main_functions"
            amo_variables
            amo_variables__licenses
            
            #            echo "$run_path/build/json/current_versions/$(jq ".results[$entry].current_version.id" "$file")"
            
            # This is the complete list of pre-defined licenses listed on AMO. Apache License 2.0 is not on the list so its not easy to detect because it has to be released under a "Custom License".
            # Make a drop-down menu with all SPDX licenses - https://github.com/mozilla/addons-server/issues/8866
            # See https://addons.mozilla.org/en-US/developers/addon/<your add-on>/ownership for the current add-on list. Note that not all licenses are added to the list.
            # DuckDuckGo Privacy Essentials is distributed under Apache License 2.0.
            # Apache License, version 2.0 is not on the add-on license list - https://github.com/mozilla/addons-server/issues/8545
            if
                [ "$license__amo_full_name" == "BSD License" ] ||
                    [ "$license__amo_full_name" == "GNU General Public License, version 3.0" ] ||
                    [ "$license__amo_full_name" == "GNU General Public License, version 2.0" ] ||
                    [ "$license__amo_full_name" == "GNU Lesser General Public License, version 3.0" ] ||
                    [ "$license__amo_full_name" == "GNU Lesser General Public License, version 2.1" ] ||
                    [ "$license__amo_full_name" == "MIT/X11 License" ] ||
                    [ "$license__amo_full_name" == "Mozilla Public License, version 2.0" ] ||
                    [ "$license__amo_full_name" == "Mozilla Public License Version 1.1" ]
            then

                # The nonfree add-ons should not be removed since the line number must correspond with the entry number in the search result JSON files.
                #                echo "$name: $license__amo_full_name"
                echo "$file $entry $slug" >> "$output_file_2"
                
                freedom_status="free";

            elif [ "$license__amo_full_name" == "All Rights Reserved" ]; then

                freedom_status="nonfree";

            else

                freedom_status="unknown";
            fi    


            
        fi

        if ! $debug; then echo "foo"; fi # for pv
        
    done < "$output_file_1" | pv__progress

}

#!/bin/bash
# Copyright 2018 Ian Kelling

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Commentary: Bash stack trace and error handling functions. This file
# is meant to be sourced. It loads some functions which you may want to
# call manually (see the comments at the start of each one), and then
# runs err-catch. See the README file for a slightly longer explanation.

err-allow() {
  # help: turn off exit and stack trace on error. undoes err-catch
  set +E +o pipefail; trap ERR
}

err-bash-trace() {
  # help: print stack trace
  #
  # Note: It does not show function args unless you first run:
  # shopt -s extdebug
  # err-catch runs this for you.

  local -i argc_index=0 frame i start=${1:-1} max_indent=8 indent
  local source
  local extdebug=false
  if [[ $(shopt -p extdebug) == *-s* ]]; then
    extdebug=true
  fi
  for ((frame=0; frame < ${#FUNCNAME[@]}-1; frame++)); do
    argc=${BASH_ARGC[frame]}
    argc_index+=$argc
    ((frame < start)) && continue
    if (( ${#BASH_SOURCE[@]} > 1 )); then
      source="${BASH_SOURCE[frame+1]}:${BASH_LINENO[frame]}:"
    fi
    indent=$((frame-start+1))
    indent=$((indent < max_indent ? indent : max_indent))
    printf "%${indent}s↳%sin \`%s" '' "$source" "${FUNCNAME[frame]}"
    if $extdebug; then
      for ((i=argc_index-1; i >= argc_index-argc; i--)); do
        printf " %s" "${BASH_ARGV[i]}"
      done
    fi
    echo \'
  done
}

err-catch() {
  # help: on errors: print stack trace and exit
  #
  # You can set "${_errcatch_cleanup[@]}" to a command and it will run before exiting.
  # This function depends on err-bash-trace.

  set -E; shopt -s extdebug
  _err-trap() {
    err=$?
    exec >&2
    set +x
    echo "${BASH_SOURCE[1]}:${BASH_LINENO[0]}:in \`$BASH_COMMAND' returned $err"
    # err trap does not work within an error trap, the following line:
    err-bash-trace 2; set -e
    "${_errcatch_cleanup[@]:-:}" # note :-: is to be compatible with set -u
    echo "$0: exiting with code $err"
    exit $err
  }
  trap _err-trap ERR
  set -o pipefail
}

err-exit() {
  # usage: err-exit [EXIT_CODE] [MESSAGE]
  # help: exit and print stack trace.
  #
  # Use this instead of the exit command to be more informative. default
  # EXIT_CODE is 1. If only one of EXIT_CODE and MESSAGE is given,
  # we consider it to be an exit code if it is a number.
  # This function depends on err-bash-trace.

  exec >&2
  code=1
  if [[ "$*" ]]; then
    if [[ ${1/[^0-9]/} == "$1" ]]; then
      code=$1
      if [[ $2 ]]; then
        printf '%s\n' "$2"
      fi
    else
      printf '%s\n' "$0: $1"
    fi
  fi
  echo "${BASH_SOURCE[1]}:${BASH_LINENO[0]}"
  err-bash-trace 2
  echo "$0: exiting with code $code"
  exit $err
}

err-catch

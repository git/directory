#####################################################################
About FreeAMO

FreeAMO is a project used to get meta-data for free add-ons at
addons.mozilla.org (AMO) used to produce free add-on repositories for
free Mozilla-based programs.  contain nonfree add-ons and Mozilla
Foundation's addons.mozilla.org contains nonfree add-ons, and their
programs including Firefox, Thunderbird, SeaMonkey, and Sunbird are
trademarked and thus nonfree.

FreeAMO ships with configuration files used to find free add-ons for
the latest icecat version for Trisquel GNU/Linux. For more about
IceCat see the `About IceCat` section and read the `About
WebExtensions` section to find out why legacy add-ons are
excluded. Icedove add-ons is not synced yet because Thunderbird still
hasn't implemented WebExtension support, but it's discussed by the
Mozilla developers, see
https://bugzilla.mozilla.org/show_bug.cgi?id=1396172 For more about
Icedove see the `About Icedove` section. FreeAMO has an option to
generate .wiki files designed to be imported to
https://directory.fsf.org/wiki/, example:
https://directory.fsf.org/wiki/LibreJS

This script uses the Mozilla add-on API
(https://addons-server.readthedocs.io/en/latest/topics/api/addons.html),
and decides what to do with add-ons distributed under a free
license. A lot of developers have added a license on the
addons.mozilla.org page for their add-on but are not aware of the
common guidelines to make their programs fully free because Mozilla
never informed them about it. Common issues:
* COPYING or LICENSE file in the root directory for example.
* There are no (full) license notices in the non-trivial source files

Add-ons that doesn't have these issues will get generated XML files
that can be synced to the Free Software Directory while add-ons that
has these issues will generated on a list so we can contact the
developers and ask them to fix them.

#####################################################################
About addons.mozilla.org

Mozilla's description: "Addons.mozilla.org (AMO), is Mozilla's
official site for discovering and installing add-ons for the Firefox
browser. Add-ons help you modify and personalize your browsing
experience by adding new features to Firefox, enhancing your
interactions with Web content, and changing the way your browser
looks.

You can also use AMO to find add-ons for Mozilla Thunderbird and
SeaMonkey." - https://addons.mozilla.org/en-US/about

API documentation: 
https://addons-server.readthedocs.io/en/latest/topics/api/addons.html

#####################################################################
About WebExtensions

GNU IceCat 60 is based on Firefox ESR 60 which is the first Firefox
ESR version based on Firefox Quantum (Firefox 57+). Firefox Quantum
removed support for legacy add-ons and only run WebExtension
add-ons. WebExtensions is a new browser extension compatible with the
extensions APIs of Google Chrome and Microsoft Edge.

#####################################################################
About GNU IceCat

IceCat is the unbranded version of Firefox ESR.

GNU IceCat aim to be based on the the current official release of
Firefox Extended Support Release (ESR) with removal of trademarked
artwork and proprietary components.

However, IceCat is not a straight fork of Firefox ESR; instead, it is
a parallel effort that works closely with and re-bases in
synchronization on the latest Firefox ESR as the upstream supplier,
with patches merged upstream whenever possible; although it should be
noted that additional security updates are customized to IceCat
occasionally.

#####################################################################
About Icedove

Icedove is the unbranded version of Thunderbird.

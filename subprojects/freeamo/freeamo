#!/usr/bin/env bash
#    This file is part of FreeAMO
#    Copyright (C) 2017, 2018 David Hedlund
#
#    FreeAMO is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    FreeAMO is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


scriptsrc=$(readlink -f -- "${BASH_SOURCE[0]}")
run_path=$(dirname "${scriptsrc}" || echo .)
filename=$(basename "$0");

source "$run_path/src/main_functions"
source "$run_path/freeamo.conf"


source "$run_path/src/err"

# if you don't have this locale, add this to /etc/locale.gen:
# sv_SE.UTF-8 UTF-8
# then run sudo locale-gen
# then open a new terminal.
export LC_NUMERIC=sv_SE.UTF-8


output_file_1="$run_path/build/index-search-pages.txt";
output_file_2="$run_path/build/index-search-pages--free-licenses.txt"

# Evaluate debug mode
if [ "$1" == "--debug" ] || [ "$2" == "--debug" ]; then
    
    debug=true
    debug_arg="--debug";
    
else
    
    debug=false
    unset debug_arg

fi

# Delete empty files to re-generate them -- Typical case scenario: 1) wget tried to download a file 2) The script is interupted 3) The script is run again
if [ -d "$run_path/build" ]; then
    
    find "$run_path/build" -type f -empty -delete

fi

export -p debug

if [ ! -f /usr/bin/jq ]; then echo "/usr/bin/jq not found!"; exit=true; fi
if [ ! -f /usr/bin/wget ]; then echo "/usr/bin/wget not found!"; exit=true; fi

if [[ $exit == true ]]; then exit 1; fi


case "$1" in

    ""|--debug|--normal-mode)

        freeamo_command="$0 --conf-note $debug_arg"
        echo "$freeamo_command"; $freeamo_command
        
        freeamo_command="$0 --remove-build $debug_arg"
        echo "$freeamo_command"; $freeamo_command

        freeamo_command="$0 --conf-copy $debug_arg"
        echo "$freeamo_command"; $freeamo_command
        
        freeamo_command="$0 --search-pages $debug_arg"
        echo "$freeamo_command"; $freeamo_command

        freeamo_command="$0 --get-licenses $debug_arg"
        echo "$freeamo_command"; $freeamo_command

        freeamo_command="$0 --get-add-ons $debug_arg"
        echo "$freeamo_command"; $freeamo_command
        
        if [ ! "$1" == "--normal-mode" ]; then
            
            freeamo_command="$0 --make-directory.fsf.org-wiki $debug_arg"
            echo "$freeamo_command"; $freeamo_command
            
        fi
        
        ;;
    
    -help)
	
	[ "$1" = "" ] && echo "Usage: $filename [--option] [--debug]

OPTIONS
f.org-wiki (they are used by Free Software Directory developers)
	$0 --conf-note
            Display which icecat version the add-on will be compatible with.
	$0 --remove-build
            Remove the build directory if it exists.
	$0 --conf-copy
            Copy the conf file to the build directory.
	$0 --search-pages
            Downloads the most popular WebExtensions from https://addons.mozilla.org/en-US/firefox/search/...
	$0 --get-licenses
            The licenses are not added to the general JSON file. This option will download the neccesary license JSON files for all add-ons.
        $0 --get-add-ons
            Download the installable add-ons (the .xpi files).
        $0 --make-directory.fsf.org-wiki
            Generates .wiki files specifically for https://directory.fsf.org/wiki/

Disabled options due to limited development time
        $0 --make-repository-list
            Required file: FreeAMO-collections
            File content syntax: https://addons.mozilla.org/en-US/firefox/collections/<user>/
        $0 --make-collection-list
            Required file: FreeAMO-collections
            File content syntax: https://addons.mozilla.org/en-US/firefox/collections/<user>/<collection name>
        $0 --make-custom-list
            Required file: FreeAMO-custom
            File content syntax: https://addons.mozilla.org/en-US/firefox/addon/<add-on name>/
        $0 --download-licenses-json
        $0 --download-free-webextensions
            Downloads the latest version of the free WebExtensions.
        $0 --verify-license-copy
            Search for license files in the root directory in the downloaded WebExtensions (.xpi files)
" && exit 1

	;;

    --conf-note)
        
        echo "$notes"

        ;;

    --remove-build)

        rm -fr "$run_path/build"

        ;;
    
    --conf-copy)
        
        mkdir -p "$run_path/build/"
        echo -e "# [This file is a copy used as a receipt to declare the settings that were used to generate the files.]\\n" > "$run_path/build/freeamo.conf"
        cat "$run_path/freeamo.conf" >> "$run_path/build/freeamo.conf"
        sed -i "/^appversion=/ s/$/ # $appversion at the time/" "$run_path/build/freeamo.conf"
        
        ;;

    --search-pages)

        # This option is named after named after https://addons.mozilla.org/en-US/firefox/search/
        freeamo_command="$0 --search-pages--download $debug_arg"
        echo "$freeamo_command"; $freeamo_command
        
        freeamo_command="$0 --search-pages--filter $debug_arg"
        echo "$freeamo_command"; $freeamo_command
        
        ;;
    
    --search-pages--download)

        mkdir -p "$run_path/build/json/search-pages"
        source src/search-pages
        search-pages--download
        
        ;;

    --search-pages--filter)

        mkdir -p "$run_path/build/json/search-pages"
        source src/search-pages
        search-pages--filter
        
        ;;
    
    --get-licenses)

        freeamo_command="$0 --get-licenses--download $debug_arg"
        echo "$freeamo_command"; $freeamo_command
        
        freeamo_command="$0 --get-licenses--filter $debug_arg"
        echo "$freeamo_command"; $freeamo_command
        
        ;;

    --get-licenses--download)

        mkdir -p "$run_path/build/json/current_versions";
        source src/get-licenses
        get-licenses--download
        
        ;;

    --get-licenses--filter)

        source src/get-licenses
        get-licenses--filter
        
        ;;

    --get-add-ons)

        freeamo_command="$0 --get-add-ons--download $debug_arg"
        echo "$freeamo_command"; $freeamo_command
        
        freeamo_command="$0 --get-add-ons--filter $debug_arg"
        echo "$freeamo_command"; $freeamo_command
        
        ;;

    --get-add-ons--download)

        mkdir -p "$run_path/build/add-ons"
        source src/get-add-ons
        get-add-ons--download
        
        ;;

    --get-add-ons--filter)

        source src/get-add-ons
        get-add-ons--filter
        
        ;;
    
    --make-directory.fsf.org-wiki)

        mkdir -p "$run_path/build/wiki";
        source src/make-wiki
        
        ;;
    
esac

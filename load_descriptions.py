#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2023 Free Software Foundation <info@fsf.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys

import debian.deb822
import pandas as pd

if __name__ == '__main__':
    descs = debian.deb822.Packages.iter_paragraphs(sys.stdin)
    df = pd.DataFrame([dict(p) for p in descs])
    store = pd.HDFStore('pkg.h5')
    store['descriptions'] = df
    store.close()

